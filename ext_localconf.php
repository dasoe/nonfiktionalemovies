<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Dasoe.Nonfiktionalemovies',
            'Movies',
            [
                'Movie' => 'list, listDay, listTitle, show'
            ],
            // non-cacheable actions
            [
                'Movie' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    movies {
                        iconIdentifier = nonfiktionalemovies-plugin-movies
                        title = LLL:EXT:nonfiktionalemovies/Resources/Private/Language/locallang_db.xlf:tx_nonfiktionalemovies_movies.name
                        description = LLL:EXT:nonfiktionalemovies/Resources/Private/Language/locallang_db.xlf:tx_nonfiktionalemovies_movies.description
                        tt_content_defValues {
                            CType = list
                            list_type = nonfiktionalemovies_movies
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'nonfiktionalemovies-plugin-movies',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:nonfiktionalemovies/Resources/Public/Icons/user_plugin_movies.svg']
			);
		
    }
);
