#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_movie'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_movie (

	title varchar(255) DEFAULT '' NOT NULL,
	image int(11) unsigned DEFAULT '0' NOT NULL,
	hauptprogramm smallint(5) unsigned DEFAULT '0' NOT NULL,
	kinderprogramm smallint(5) unsigned DEFAULT '0' NOT NULL,
	jungedoks smallint(5) unsigned DEFAULT '0' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	year varchar(255) DEFAULT '' NOT NULL,
	length varchar(255) DEFAULT '' NOT NULL,
	omu smallint(5) unsigned DEFAULT '0' NOT NULL,
	involved text,
	description text,
	prices text,
	collection int(11) unsigned DEFAULT '0' NOT NULL,
	presentation int(11) unsigned DEFAULT '0' NOT NULL,
	video int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_day'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_day (

	date date DEFAULT NULL,

);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_presentation'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_presentation (

	movie int(11) unsigned DEFAULT '0' NOT NULL,
	collection int(11) unsigned DEFAULT '0' NOT NULL,

	time int(11) DEFAULT '0' NOT NULL,
	day int(11) unsigned DEFAULT '0',
	branch int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_video'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_video (

	movie int(11) unsigned DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	still int(11) unsigned NOT NULL default '0',
	mp4source int(11) unsigned NOT NULL default '0',
	webmsource int(11) unsigned NOT NULL default '0',
	oggsource int(11) unsigned NOT NULL default '0',

);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_collection'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_collection (

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	image int(11) unsigned NOT NULL default '0',
	list_description text,
	presentation int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_branch'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_branch (

	title varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_presentation'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_presentation (

	movie int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_video'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_video (

	movie int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_nonfiktionalemovies_movie_collection_mm'
#
CREATE TABLE tx_nonfiktionalemovies_movie_collection_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_nonfiktionalemovies_presentation_branch_mm'
#
CREATE TABLE tx_nonfiktionalemovies_presentation_branch_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_nonfiktionalemovies_domain_model_presentation'
#
CREATE TABLE tx_nonfiktionalemovies_domain_model_presentation (

	collection int(11) unsigned DEFAULT '0' NOT NULL,

);
