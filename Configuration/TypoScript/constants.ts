
plugin.tx_nonfiktionalemovies_movies {
    view {
        # cat=plugin.tx_nonfiktionalemovies_movies/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:nonfiktionalemovies/Resources/Private/Templates/
        # cat=plugin.tx_nonfiktionalemovies_movies/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:nonfiktionalemovies/Resources/Private/Partials/
        # cat=plugin.tx_nonfiktionalemovies_movies/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:nonfiktionalemovies/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_nonfiktionalemovies_movies//a; type=string; label=Default storage PID
        storagePid =
    }
}

module.tx_nonfiktionalemovies_nonfiktionaleprogram {
    view {
        # cat=module.tx_nonfiktionalemovies_nonfiktionaleprogram/file; type=string; label=Path to template root (BE)
        templateRootPath = EXT:nonfiktionalemovies/Resources/Private/Backend/Templates/
        # cat=module.tx_nonfiktionalemovies_nonfiktionaleprogram/file; type=string; label=Path to template partials (BE)
        partialRootPath = EXT:nonfiktionalemovies/Resources/Private/Backend/Partials/
        # cat=module.tx_nonfiktionalemovies_nonfiktionaleprogram/file; type=string; label=Path to template layouts (BE)
        layoutRootPath = EXT:nonfiktionalemovies/Resources/Private/Backend/Layouts/
    }
    persistence {
        # cat=module.tx_nonfiktionalemovies_nonfiktionaleprogram//a; type=string; label=Default storage PID
        storagePid =
    }
}
