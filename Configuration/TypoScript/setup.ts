page.includeJSFooter {  
    nonfiktionalemovies = EXT:nonfiktionalemovies/Resources/Public/js/nonfiktionalemovies.js   
    #nonfiktionalemoviesCollapse = EXT:{extension.extensionKey}/Resources/Public/js/nonfiktionalemoviesCollapse.js   
    swiperSlider = EXT:nonfiktionalemovies/Resources/Public/js/swiper.min.js 
    zzzz_swiperInitialize = EXT:nonfiktionalemovies/Resources/Public/js/swiperInitialize.js 
}

page.includeCSS { 
    swiper = EXT:nonfiktionalemovies/Resources/Public/css/swiper.min.css 
}