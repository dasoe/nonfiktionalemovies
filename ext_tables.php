<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Dasoe.Nonfiktionalemovies',
            'Movies',
            'show movies'
        );

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Dasoe.Nonfiktionalemovies',
                'web', // Make module a submodule of 'web'
                'nonfiktionaleprogram', // Submodule key
                '', // Position
                [
                    'Movie' => 'list, show',
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:nonfiktionalemovies/Resources/Public/Icons/user_mod_nonfiktionaleprogram.svg',
                    'labels' => 'LLL:EXT:nonfiktionalemovies/Resources/Private/Language/locallang_nonfiktionaleprogram.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('nonfiktionalemovies', 'Configuration/TypoScript', 'nonfiktionale movies');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nonfiktionalemovies_domain_model_movie', 'EXT:nonfiktionalemovies/Resources/Private/Language/locallang_csh_tx_nonfiktionalemovies_domain_model_movie.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nonfiktionalemovies_domain_model_movie');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nonfiktionalemovies_domain_model_day', 'EXT:nonfiktionalemovies/Resources/Private/Language/locallang_csh_tx_nonfiktionalemovies_domain_model_day.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nonfiktionalemovies_domain_model_day');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nonfiktionalemovies_domain_model_presentation', 'EXT:nonfiktionalemovies/Resources/Private/Language/locallang_csh_tx_nonfiktionalemovies_domain_model_presentation.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nonfiktionalemovies_domain_model_presentation');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nonfiktionalemovies_domain_model_video', 'EXT:nonfiktionalemovies/Resources/Private/Language/locallang_csh_tx_nonfiktionalemovies_domain_model_video.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nonfiktionalemovies_domain_model_video');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nonfiktionalemovies_domain_model_collection', 'EXT:nonfiktionalemovies/Resources/Private/Language/locallang_csh_tx_nonfiktionalemovies_domain_model_collection.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nonfiktionalemovies_domain_model_collection');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nonfiktionalemovies_domain_model_branch', 'EXT:nonfiktionalemovies/Resources/Private/Language/locallang_csh_tx_nonfiktionalemovies_domain_model_branch.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nonfiktionalemovies_domain_model_branch');

    }
);
