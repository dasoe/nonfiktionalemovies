<?php
namespace Dasoe\Nonfiktionalemovies\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class VideoTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Nonfiktionalemovies\Domain\Model\Video
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Nonfiktionalemovies\Domain\Model\Video();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStillReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getStill()
        );
    }

    /**
     * @test
     */
    public function setStillForFileReferenceSetsStill()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setStill($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'still',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMp4sourceReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getMp4source()
        );
    }

    /**
     * @test
     */
    public function setMp4sourceForFileReferenceSetsMp4source()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setMp4source($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'mp4source',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWebmsourceReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getWebmsource()
        );
    }

    /**
     * @test
     */
    public function setWebmsourceForFileReferenceSetsWebmsource()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setWebmsource($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'webmsource',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOggsourceReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getOggsource()
        );
    }

    /**
     * @test
     */
    public function setOggsourceForFileReferenceSetsOggsource()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setOggsource($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'oggsource',
            $this->subject
        );
    }
}
