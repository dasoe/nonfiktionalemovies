<?php
namespace Dasoe\Nonfiktionalemovies\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class DateTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Nonfiktionalemovies\Domain\Model\Date
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Nonfiktionalemovies\Domain\Model\Date();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
