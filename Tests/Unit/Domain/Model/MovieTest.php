<?php
namespace Dasoe\Nonfiktionalemovies\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class MovieTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Nonfiktionalemovies\Domain\Model\Movie
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Nonfiktionalemovies\Domain\Model\Movie();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $objectStorageHoldingExactlyOneImage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneImage->attach($image);
        $this->subject->setImage($objectStorageHoldingExactlyOneImage);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneImage,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addImageToObjectStorageHoldingImage()
    {
        $image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $imageObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $imageObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($image));
        $this->inject($this->subject, 'image', $imageObjectStorageMock);

        $this->subject->addImage($image);
    }

    /**
     * @test
     */
    public function removeImageFromObjectStorageHoldingImage()
    {
        $image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $imageObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $imageObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($image));
        $this->inject($this->subject, 'image', $imageObjectStorageMock);

        $this->subject->removeImage($image);
    }

    /**
     * @test
     */
    public function getHauptprogrammReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getHauptprogramm()
        );
    }

    /**
     * @test
     */
    public function setHauptprogrammForBoolSetsHauptprogramm()
    {
        $this->subject->setHauptprogramm(true);

        self::assertAttributeEquals(
            true,
            'hauptprogramm',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getKinderprogrammReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getKinderprogramm()
        );
    }

    /**
     * @test
     */
    public function setKinderprogrammForBoolSetsKinderprogramm()
    {
        $this->subject->setKinderprogramm(true);

        self::assertAttributeEquals(
            true,
            'kinderprogramm',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getJungedoksReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getJungedoks()
        );
    }

    /**
     * @test
     */
    public function setJungedoksForBoolSetsJungedoks()
    {
        $this->subject->setJungedoks(true);

        self::assertAttributeEquals(
            true,
            'jungedoks',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCountryReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCountry()
        );
    }

    /**
     * @test
     */
    public function setCountryForStringSetsCountry()
    {
        $this->subject->setCountry('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'country',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getYearReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getYear()
        );
    }

    /**
     * @test
     */
    public function setYearForStringSetsYear()
    {
        $this->subject->setYear('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'year',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLengthReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLength()
        );
    }

    /**
     * @test
     */
    public function setLengthForStringSetsLength()
    {
        $this->subject->setLength('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'length',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOmuReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getOmu()
        );
    }

    /**
     * @test
     */
    public function setOmuForBoolSetsOmu()
    {
        $this->subject->setOmu(true);

        self::assertAttributeEquals(
            true,
            'omu',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInvolvedReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInvolved()
        );
    }

    /**
     * @test
     */
    public function setInvolvedForStringSetsInvolved()
    {
        $this->subject->setInvolved('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'involved',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPricesReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPrices()
        );
    }

    /**
     * @test
     */
    public function setPricesForStringSetsPrices()
    {
        $this->subject->setPrices('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'prices',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCollectionReturnsInitialValueForCollection()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getCollection()
        );
    }

    /**
     * @test
     */
    public function setCollectionForObjectStorageContainingCollectionSetsCollection()
    {
        $collection = new \Dasoe\Nonfiktionalemovies\Domain\Model\Collection();
        $objectStorageHoldingExactlyOneCollection = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneCollection->attach($collection);
        $this->subject->setCollection($objectStorageHoldingExactlyOneCollection);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneCollection,
            'collection',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addCollectionToObjectStorageHoldingCollection()
    {
        $collection = new \Dasoe\Nonfiktionalemovies\Domain\Model\Collection();
        $collectionObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $collectionObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($collection));
        $this->inject($this->subject, 'collection', $collectionObjectStorageMock);

        $this->subject->addCollection($collection);
    }

    /**
     * @test
     */
    public function removeCollectionFromObjectStorageHoldingCollection()
    {
        $collection = new \Dasoe\Nonfiktionalemovies\Domain\Model\Collection();
        $collectionObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $collectionObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($collection));
        $this->inject($this->subject, 'collection', $collectionObjectStorageMock);

        $this->subject->removeCollection($collection);
    }

    /**
     * @test
     */
    public function getPresentationReturnsInitialValueForPresentation()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPresentation()
        );
    }

    /**
     * @test
     */
    public function setPresentationForObjectStorageContainingPresentationSetsPresentation()
    {
        $presentation = new \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation();
        $objectStorageHoldingExactlyOnePresentation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePresentation->attach($presentation);
        $this->subject->setPresentation($objectStorageHoldingExactlyOnePresentation);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOnePresentation,
            'presentation',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addPresentationToObjectStorageHoldingPresentation()
    {
        $presentation = new \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation();
        $presentationObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $presentationObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($presentation));
        $this->inject($this->subject, 'presentation', $presentationObjectStorageMock);

        $this->subject->addPresentation($presentation);
    }

    /**
     * @test
     */
    public function removePresentationFromObjectStorageHoldingPresentation()
    {
        $presentation = new \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation();
        $presentationObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $presentationObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($presentation));
        $this->inject($this->subject, 'presentation', $presentationObjectStorageMock);

        $this->subject->removePresentation($presentation);
    }

    /**
     * @test
     */
    public function getVideoReturnsInitialValueForVideo()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getVideo()
        );
    }

    /**
     * @test
     */
    public function setVideoForObjectStorageContainingVideoSetsVideo()
    {
        $video = new \Dasoe\Nonfiktionalemovies\Domain\Model\Video();
        $objectStorageHoldingExactlyOneVideo = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneVideo->attach($video);
        $this->subject->setVideo($objectStorageHoldingExactlyOneVideo);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneVideo,
            'video',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addVideoToObjectStorageHoldingVideo()
    {
        $video = new \Dasoe\Nonfiktionalemovies\Domain\Model\Video();
        $videoObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $videoObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($video));
        $this->inject($this->subject, 'video', $videoObjectStorageMock);

        $this->subject->addVideo($video);
    }

    /**
     * @test
     */
    public function removeVideoFromObjectStorageHoldingVideo()
    {
        $video = new \Dasoe\Nonfiktionalemovies\Domain\Model\Video();
        $videoObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $videoObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($video));
        $this->inject($this->subject, 'video', $videoObjectStorageMock);

        $this->subject->removeVideo($video);
    }
}
