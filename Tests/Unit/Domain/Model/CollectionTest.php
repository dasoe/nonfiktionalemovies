<?php
namespace Dasoe\Nonfiktionalemovies\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class CollectionTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Nonfiktionalemovies\Domain\Model\Collection
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Nonfiktionalemovies\Domain\Model\Collection();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getListDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getListDescription()
        );
    }

    /**
     * @test
     */
    public function setListDescriptionForStringSetsListDescription()
    {
        $this->subject->setListDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'listDescription',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPresentationReturnsInitialValueForPresentation()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPresentation()
        );
    }

    /**
     * @test
     */
    public function setPresentationForObjectStorageContainingPresentationSetsPresentation()
    {
        $presentation = new \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation();
        $objectStorageHoldingExactlyOnePresentation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePresentation->attach($presentation);
        $this->subject->setPresentation($objectStorageHoldingExactlyOnePresentation);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOnePresentation,
            'presentation',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addPresentationToObjectStorageHoldingPresentation()
    {
        $presentation = new \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation();
        $presentationObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $presentationObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($presentation));
        $this->inject($this->subject, 'presentation', $presentationObjectStorageMock);

        $this->subject->addPresentation($presentation);
    }

    /**
     * @test
     */
    public function removePresentationFromObjectStorageHoldingPresentation()
    {
        $presentation = new \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation();
        $presentationObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $presentationObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($presentation));
        $this->inject($this->subject, 'presentation', $presentationObjectStorageMock);

        $this->subject->removePresentation($presentation);
    }
}
