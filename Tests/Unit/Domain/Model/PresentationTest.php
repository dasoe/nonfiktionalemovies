<?php
namespace Dasoe\Nonfiktionalemovies\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class PresentationTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTimeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getTime()
        );
    }

    /**
     * @test
     */
    public function setTimeForIntSetsTime()
    {
        $this->subject->setTime(12);

        self::assertAttributeEquals(
            12,
            'time',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDayReturnsInitialValueForDay()
    {
        self::assertEquals(
            null,
            $this->subject->getDay()
        );
    }

    /**
     * @test
     */
    public function setDayForDaySetsDay()
    {
        $dayFixture = new \Dasoe\Nonfiktionalemovies\Domain\Model\Day();
        $this->subject->setDay($dayFixture);

        self::assertAttributeEquals(
            $dayFixture,
            'day',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBranchReturnsInitialValueForBranch()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getBranch()
        );
    }

    /**
     * @test
     */
    public function setBranchForObjectStorageContainingBranchSetsBranch()
    {
        $branch = new \Dasoe\Nonfiktionalemovies\Domain\Model\Branch();
        $objectStorageHoldingExactlyOneBranch = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneBranch->attach($branch);
        $this->subject->setBranch($objectStorageHoldingExactlyOneBranch);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneBranch,
            'branch',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addBranchToObjectStorageHoldingBranch()
    {
        $branch = new \Dasoe\Nonfiktionalemovies\Domain\Model\Branch();
        $branchObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $branchObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($branch));
        $this->inject($this->subject, 'branch', $branchObjectStorageMock);

        $this->subject->addBranch($branch);
    }

    /**
     * @test
     */
    public function removeBranchFromObjectStorageHoldingBranch()
    {
        $branch = new \Dasoe\Nonfiktionalemovies\Domain\Model\Branch();
        $branchObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $branchObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($branch));
        $this->inject($this->subject, 'branch', $branchObjectStorageMock);

        $this->subject->removeBranch($branch);
    }
}
