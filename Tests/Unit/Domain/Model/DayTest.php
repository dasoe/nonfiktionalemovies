<?php
namespace Dasoe\Nonfiktionalemovies\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class DayTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Nonfiktionalemovies\Domain\Model\Day
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Nonfiktionalemovies\Domain\Model\Day();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDate()
        );
    }

    /**
     * @test
     */
    public function setDateForDateTimeSetsDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'date',
            $this->subject
        );
    }
}
