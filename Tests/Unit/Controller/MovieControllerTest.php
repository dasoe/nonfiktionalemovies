<?php
namespace Dasoe\Nonfiktionalemovies\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author das oe <christian.oettinger@gmx.de>
 */
class MovieControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Dasoe\Nonfiktionalemovies\Controller\MovieController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Dasoe\Nonfiktionalemovies\Controller\MovieController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllMoviesFromRepositoryAndAssignsThemToView()
    {

        $allMovies = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $movieRepository = $this->getMockBuilder(\Dasoe\Nonfiktionalemovies\Domain\Repository\MovieRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $movieRepository->expects(self::once())->method('findAll')->will(self::returnValue($allMovies));
        $this->inject($this->subject, 'movieRepository', $movieRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('movies', $allMovies);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenMovieToView()
    {
        $movie = new \Dasoe\Nonfiktionalemovies\Domain\Model\Movie();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('movie', $movie);

        $this->subject->showAction($movie);
    }
}
