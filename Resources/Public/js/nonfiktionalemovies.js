/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    // console.log("js nonfiktionalemovies included");

    $('.oneViewInput input').click(function () {
        window.location.href = $(this).parent().parent().find('.oneViewLabel a').attr('href');
    });

    $('.oneFilter').click(function () {
        var whichFilterNr = $(this).data('filternr');
        console.log(whichFilterNr);

        if ($(this).data('filteron')) {
            $('.oneMovie').each(function (index, value) {
                if ($(this).data('filter') == whichFilterNr) {
                    $(this).fadeOut();
                }
            });
            $(this).data('filteron', 0);
        } else {
            $('.oneMovie').each(function (index, value) {
                if ($(this).data('filter') == whichFilterNr) {
                    $(this).fadeIn();
                }
            });
            $(this).data('filteron', 1);
        }

    });
});
