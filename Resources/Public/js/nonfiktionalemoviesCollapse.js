$(document).ready(function() {  

    /* ---------------------------------------------------
     * ----- klappinhalte versteckt und durchsichtig: ----
     * ----- Produktseite und Content Element ------------
     *
      --------------------------------------------------- */
     
    $('.collapsableContainer').hide();
    $('.collapsableContainer').css({ opacity: 0});

    $(".clickableHeader").click(function() {
        var clickableHeaderObj = $(this);
        //$(this).parent().next('.csc-textpic-text').toggle();
        if (clickableHeaderObj.hasClass('open') ){
               //$(this).parent().next('.csc-textpic-text').fadeOut(850, "linear");
            $(this).nextAll('.collapsableContainer').first().animate({
                 opacity: 0,
                 left: '+=50',
                 height: 'toggle',
                 opacity: 1
                }, 450, function() {
                // Animation complete.
                console.log("remove class...");
                $(this).removeClass("open");
                clickableHeaderObj.removeClass("open");
            });
            
        }else{
            $(this).nextAll('.collapsableContainer').first().animate({
                 opacity: 1,
                 left: '+=50',
                 height: 'toggle'
                }, 450, function() {
                // Animation complete.
                console.log("add class...");
                
                $(this).addClass("open");
                clickableHeaderObj.addClass("open");
                
            });
            
            
          }
    });
    
});
