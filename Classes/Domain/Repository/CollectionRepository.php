<?php
namespace Dasoe\Nonfiktionalemovies\Domain\Repository;


/***
 *
 * This file is part of the "nonfiktionale movies" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * The repository for Collections
 */
class CollectionRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Returns all available objects: default up to one year after create, if availableUntil is set, availableUntil
     * 
     * @param $day Dasoe\Nonfiktionalemovies\Domain\Model\Day
     * @return array An array of objects, empty if no objects found
     */
    public function findByDay($day)
    {
        $query = $this->createQuery();
        $query->setOrderings(
        [
    'presentation.time' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
]
        );
        $query->matching($query->equals('presentation.day', $day));
        return $query->execute();
    }
}
