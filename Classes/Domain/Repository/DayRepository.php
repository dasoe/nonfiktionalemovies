<?php
namespace Dasoe\Nonfiktionalemovies\Domain\Repository;


/***
 *
 * This file is part of the "nonfiktionale movies" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * The repository for Days
 */
class DayRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Returns all objects of this repository.
     * 
     * @return QueryResultInterface|array
     */
    public function findAllArray()
    {
        return $this->createQuery()->execute()->toArray();
    }
}
