<?php
namespace Dasoe\Nonfiktionalemovies\Domain\Model;


/***
 *
 * This file is part of the "nonfiktionale movies" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Video
 */
class Video extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * still
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $still = null;

    /**
     * mp4source
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $mp4source = null;

    /**
     * webmsource
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $webmsource = null;

    /**
     * oggsource
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $oggsource = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the still
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $still
     */
    public function getStill()
    {
        return $this->still;
    }

    /**
     * Sets the still
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $still
     * @return void
     */
    public function setStill(\TYPO3\CMS\Extbase\Domain\Model\FileReference $still)
    {
        $this->still = $still;
    }

    /**
     * Returns the mp4source
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $mp4source
     */
    public function getMp4source()
    {
        return $this->mp4source;
    }

    /**
     * Sets the mp4source
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $mp4source
     * @return void
     */
    public function setMp4source(\TYPO3\CMS\Extbase\Domain\Model\FileReference $mp4source)
    {
        $this->mp4source = $mp4source;
    }

    /**
     * Returns the webmsource
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $webmsource
     */
    public function getWebmsource()
    {
        return $this->webmsource;
    }

    /**
     * Sets the webmsource
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $webmsource
     * @return void
     */
    public function setWebmsource(\TYPO3\CMS\Extbase\Domain\Model\FileReference $webmsource)
    {
        $this->webmsource = $webmsource;
    }

    /**
     * Returns the oggsource
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $oggsource
     */
    public function getOggsource()
    {
        return $this->oggsource;
    }

    /**
     * Sets the oggsource
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $oggsource
     * @return void
     */
    public function setOggsource(\TYPO3\CMS\Extbase\Domain\Model\FileReference $oggsource)
    {
        $this->oggsource = $oggsource;
    }
}
