<?php
namespace Dasoe\Nonfiktionalemovies\Domain\Model;


/***
 *
 * This file is part of the "nonfiktionale movies" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Collection
 */
class Collection extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * presentation
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Movie>
     */
    protected $movie = null;

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * image
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * listDescription
     * 
     * @var string
     */
    protected $listDescription = '';

    /**
     * presentation
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation>
     * @cascade remove
     */
    protected $presentation = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the image
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the listDescription
     * 
     * @return string $listDescription
     */
    public function getListDescription()
    {
        return $this->listDescription;
    }

    /**
     * Sets the listDescription
     * 
     * @param string $listDescription
     * @return void
     */
    public function setListDescription($listDescription)
    {
        $this->listDescription = $listDescription;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->presentation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Presentation
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentation
     * @return void
     */
    public function addPresentation(\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentation)
    {
        $this->presentation->attach($presentation);
    }

    /**
     * Removes a Presentation
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentationToRemove The Presentation to be removed
     * @return void
     */
    public function removePresentation(\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentationToRemove)
    {
        $this->presentation->detach($presentationToRemove);
    }

    /**
     * Returns the presentation
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation> $presentation
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Sets the presentation
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation> $presentation
     * @return void
     */
    public function setPresentation(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $presentation)
    {
        $this->presentation = $presentation;
    }

    /**
     * Adds a Movie
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Movie $movie
     * @return void
     */
    public function addMovie(\Dasoe\Nonfiktionalemovies\Domain\Model\Movie $movie)
    {
        $this->movie->attach($movie);
    }

    /**
     * Removes a Movie
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Movie $movieToRemove The Movie to be removed
     * @return void
     */
    public function removeMovie(\Dasoe\Nonfiktionalemovies\Domain\Model\Movie $movieToRemove)
    {
        $this->movie->detach($movieToRemove);
    }

    /**
     * Returns the movie
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Movie> $movie
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Sets the movie
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Movie> $movie
     * @return void
     */
    public function setMovie(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $movie)
    {
        $this->movie = $movie;
    }
}
