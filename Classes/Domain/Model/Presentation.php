<?php
namespace Dasoe\Nonfiktionalemovies\Domain\Model;


/***
 *
 * This file is part of the "nonfiktionale movies" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Presentation
 */
class Presentation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * time
     * 
     * @var int
     */
    protected $time = 0;

    /**
     * day
     * 
     * @var \Dasoe\Nonfiktionalemovies\Domain\Model\Day
     */
    protected $day = null;

    /**
     * branch
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Branch>
     */
    protected $branch = null;

    /**
     * Returns the time
     * 
     * @return int $time
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Sets the time
     * 
     * @param int $time
     * @return void
     */
    public function setTime(int $time)
    {
        $this->time = $time;
    }

    /**
     * Returns the day
     * 
     * @return \Dasoe\Nonfiktionalemovies\Domain\Model\Day $day
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Sets the day
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Day $day
     * @return void
     */
    public function setDay(\Dasoe\Nonfiktionalemovies\Domain\Model\Day $day)
    {
        $this->day = $day;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->branch = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Branch
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Branch $branch
     * @return void
     */
    public function addBranch(\Dasoe\Nonfiktionalemovies\Domain\Model\Branch $branch)
    {
        $this->branch->attach($branch);
    }

    /**
     * Removes a Branch
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Branch $branchToRemove The Branch to be removed
     * @return void
     */
    public function removeBranch(\Dasoe\Nonfiktionalemovies\Domain\Model\Branch $branchToRemove)
    {
        $this->branch->detach($branchToRemove);
    }

    /**
     * Returns the branch
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Branch> $branch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * Sets the branch
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Branch> $branch
     * @return void
     */
    public function setBranch(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $branch)
    {
        $this->branch = $branch;
    }
}
