<?php
namespace Dasoe\Nonfiktionalemovies\Domain\Model;


/***
 *
 * This file is part of the "nonfiktionale movies" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * Movie
 */
class Movie extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * image
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @cascade remove
     */
    protected $image = null;

    /**
     * kinderprogramm
     * 
     * @var bool
     */
    protected $kinderprogramm = false;

    /**
     * jungedoks
     * 
     * @var bool
     */
    protected $jungedoks = '';

    /**
     * country
     * 
     * @var string
     */
    protected $country = '';

    /**
     * year
     * 
     * @var string
     */
    protected $year = '';

    /**
     * length
     * 
     * @var string
     */
    protected $length = '';

    /**
     * omu
     * 
     * @var bool
     */
    protected $omu = false;

    /**
     * involved
     * 
     * @var string
     */
    protected $involved = '';

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * prices
     * 
     * @var string
     */
    protected $prices = '';

    /**
     * collection
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Collection>
     */
    protected $collection = null;

    /**
     * presentation
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation>
     * @cascade remove
     */
    protected $presentation = null;

    /**
     * video
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Video>
     * @cascade remove
     */
    protected $video = null;

    /**
     * hauptprogramm
     * 
     * @var bool
     */
    protected $hauptprogramm = false;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the country
     * 
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     * 
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the year
     * 
     * @return string $year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Sets the year
     * 
     * @param string $year
     * @return void
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Returns the length
     * 
     * @return string $length
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Sets the length
     * 
     * @param string $length
     * @return void
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * Returns the omu
     * 
     * @return bool $omu
     */
    public function getOmu()
    {
        return $this->omu;
    }

    /**
     * Sets the omu
     * 
     * @param bool $omu
     * @return void
     */
    public function setOmu($omu)
    {
        $this->omu = $omu;
    }

    /**
     * Returns the boolean state of omu
     * 
     * @return bool
     */
    public function isOmu()
    {
        return $this->omu;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->image = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->collection = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->presentation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->video = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the involved
     * 
     * @return string $involved
     */
    public function getInvolved()
    {
        return $this->involved;
    }

    /**
     * Sets the involved
     * 
     * @param string $involved
     * @return void
     */
    public function setInvolved($involved)
    {
        $this->involved = $involved;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Adds a Presentation
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentation
     * @return void
     */
    public function addPresentation(\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentation)
    {
        $this->presentation->attach($presentation);
    }

    /**
     * Removes a Presentation
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentationToRemove The Presentation to be removed
     * @return void
     */
    public function removePresentation(\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation $presentationToRemove)
    {
        $this->presentation->detach($presentationToRemove);
    }

    /**
     * Returns the presentation
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation> $presentation
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Sets the presentation
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Presentation> $presentation
     * @return void
     */
    public function setPresentation(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $presentation)
    {
        $this->presentation = $presentation;
    }

    /**
     * Returns the prices
     * 
     * @return string $prices
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Sets the prices
     * 
     * @param string $prices
     * @return void
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    /**
     * Adds a Video
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Video $video
     * @return void
     */
    public function addVideo(\Dasoe\Nonfiktionalemovies\Domain\Model\Video $video)
    {
        $this->video->attach($video);
    }

    /**
     * Removes a Video
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Video $videoToRemove The Video to be removed
     * @return void
     */
    public function removeVideo(\Dasoe\Nonfiktionalemovies\Domain\Model\Video $videoToRemove)
    {
        $this->video->detach($videoToRemove);
    }

    /**
     * Returns the video
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Video> $video
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Sets the video
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Video> $video
     * @return void
     */
    public function setVideo(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $video)
    {
        $this->video = $video;
    }

    /**
     * Returns the kinderprogramm
     * 
     * @return bool kinderprogramm
     */
    public function getKinderprogramm()
    {
        return $this->kinderprogramm;
    }

    /**
     * Sets the kinderprogramm
     * 
     * @param string $kinderprogramm
     * @return void
     */
    public function setKinderprogramm($kinderprogramm)
    {
        $this->kinderprogramm = $kinderprogramm;
    }

    /**
     * Returns the jungedoks
     * 
     * @return bool jungedoks
     */
    public function getJungedoks()
    {
        return $this->jungedoks;
    }

    /**
     * Sets the jungedoks
     * 
     * @param string $jungedoks
     * @return void
     */
    public function setJungedoks($jungedoks)
    {
        $this->jungedoks = $jungedoks;
    }

    /**
     * Adds a Collection
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Collection $collection
     * @return void
     */
    public function addCollection(\Dasoe\Nonfiktionalemovies\Domain\Model\Collection $collection)
    {
        $this->collection->attach($collection);
    }

    /**
     * Removes a Collection
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Collection $collectionToRemove The Collection to be removed
     * @return void
     */
    public function removeCollection(\Dasoe\Nonfiktionalemovies\Domain\Model\Collection $collectionToRemove)
    {
        $this->collection->detach($collectionToRemove);
    }

    /**
     * Returns the collection
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Collection> $collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Sets the collection
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dasoe\Nonfiktionalemovies\Domain\Model\Collection> $collection
     * @return void
     */
    public function setCollection(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $collection)
    {
        $this->collection = $collection;
    }

    /**
     * Adds a FileReference
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function addImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image->attach($image);
    }

    /**
     * Removes a FileReference
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The FileReference to be removed
     * @return void
     */
    public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove)
    {
        $this->image->detach($imageToRemove);
    }

    /**
     * Returns the image
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the boolean state of kinderprogramm
     * 
     * @return bool kinderprogramm
     */
    public function isKinderprogramm()
    {
        return $this->kinderprogramm;
    }

    /**
     * Returns the hauptprogramm
     * 
     * @return bool $hauptprogramm
     */
    public function getHauptprogramm()
    {
        return $this->hauptprogramm;
    }

    /**
     * Sets the hauptprogramm
     * 
     * @param bool $hauptprogramm
     * @return void
     */
    public function setHauptprogramm($hauptprogramm)
    {
        $this->hauptprogramm = $hauptprogramm;
    }

    /**
     * Returns the boolean state of hauptprogramm
     * 
     * @return bool
     */
    public function isHauptprogramm()
    {
        return $this->hauptprogramm;
    }
}
