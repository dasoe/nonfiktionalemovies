<?php
namespace Dasoe\Nonfiktionalemovies\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "nonfiktionale movies" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 das oe <christian.oettinger@gmx.de>
 *
 ***/
/**
 * MovieController
 */
class MovieController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * movieRepository
     * 
     * @var \Dasoe\Nonfiktionalemovies\Domain\Repository\MovieRepository
     * @inject
     */
    protected $movieRepository = null;

    /**
     * collectionRepository
     * 
     * @var \Dasoe\Nonfiktionalemovies\Domain\Repository\CollectionRepository
     * @inject
     */
    protected $collectionRepository = null;

    /**
     * dayRepository
     * 
     * @var \Dasoe\Nonfiktionalemovies\Domain\Repository\DayRepository
     * @inject
     */
    protected $dayRepository = null;

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $days = $this->dayRepository->findAllArray();
        $preparedDays = [];
        foreach ($days as $day) {
            $preparedDays[$day->getUid()]['date'] = $day->getDate();
            $movies = $this->movieRepository->findByDay($day);
            $tempArray = [];
            $tempArray['date'] = $day->getDate();
            foreach ($movies as $movie) {
                foreach ($movie->getPresentation() as $presentation) {
                    if ($presentation->getDay() == $day) {
                        $tempArray[$presentation->getTime()]['movies'][] = $movie;

                        //DebuggerUtility::var_dump($movie);
                    }
                }
            }
            $collections = $this->collectionRepository->findByDay($day);
            foreach ($collections as $collection) {
                foreach ($collection->getPresentation() as $presentation) {
                    if ($presentation->getDay() == $day) {
                        $tempArray[$presentation->getTime()]['collections'][] = $collection;

                        //DebuggerUtility::var_dump($movie);
                    }
                }
            }
            ksort($tempArray);
            $preparedDays[$day->getUid()] = $tempArray;
        }

        //DebuggerUtility::var_dump($preparedDays);
        $this->view->assign('action', $this->request->getControllerActionName());
        $this->view->assign('preparedDays', $preparedDays);
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listDayAction()
    {
        $days = $this->dayRepository->findAllArray();
        $preparedDays = [];
        foreach ($days as $day) {
            $preparedDays[$day->getUid()]['date'] = $day->getDate();
            $movies = $this->movieRepository->findByDay($day);
            $tempArray = [];
            $tempArray['date'] = $day->getDate();
            foreach ($movies as $movie) {
                foreach ($movie->getPresentation() as $presentation) {
                    if ($presentation->getDay() == $day) {
                        $tempArray[$presentation->getTime()]['movies'][] = $movie;

                        //DebuggerUtility::var_dump($movie);
                    }
                }
            }
            $collections = $this->collectionRepository->findByDay($day);
            foreach ($collections as $collection) {
                foreach ($collection->getPresentation() as $presentation) {
                    if ($presentation->getDay() == $day) {
                        $tempArray[$presentation->getTime()]['collections'][] = $collection;

                        //DebuggerUtility::var_dump($movie);
                    }
                }
            }
            ksort($tempArray);
            $preparedDays[$day->getUid()] = $tempArray;
        }

        //DebuggerUtility::var_dump($preparedDays);
        $this->view->assign('action', $this->request->getControllerActionName());
        $this->view->assign('preparedDays', $preparedDays);
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listTitleAction()
    {
        $movies = $this->movieRepository->findAlphabetical();
        $this->view->assign('action', $this->request->getControllerActionName());
        $this->view->assign('movies', $movies);
    }

    /**
     * action show
     * 
     * @param \Dasoe\Nonfiktionalemovies\Domain\Model\Movie $movie
     * @return void
     */
    public function showAction(\Dasoe\Nonfiktionalemovies\Domain\Model\Movie $movie)
    {
        $this->view->assign('movie', $movie);
    }
}
